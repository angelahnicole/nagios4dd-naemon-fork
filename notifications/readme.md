# FORK Details

These scripts have been modified to allow the user to pass in the expected **Nagios Environment Variables**. This is to ensure compatibility with forks of Nagios such as Naemon that does not allow you to enable environment macros.

## Config File

The `config.json.example` file should be copied and written to `config.json`. This file will be ignored by source control.

```json
{
   "mail_sender"  : "Nagios Monitoring <nagiosadmin@frank4dd.com>",
   "nagios_cgiurl": "http://nagios.fm4dd.com/nagios/cgi-bin",
   "test_host"      : "susie114",
   "test_service"   : "os_cpu_load",
   "smtphost"       : "127.0.0.1",
   "domain"         : "@fm4dd.com",
   "listaddress"    : [],
   "logofile"       : "/srv/www/std-root/nagios.fm4dd.com/images/nagios-mail.gif",
   "jpg_workaround" : null,
   "pnp4nagios":
   {
      "pnp4nagios_url" : "http://nagios.fm4dd.com/pnp4nagios",
      "graph_history"  : 48,
      "pnp4nagios_auth": null,
      "server_port"    : null,
      "auth_name"      : null,
      "web_user"       : null,
      "web_pass"       : null
   },
   "nagiosgraph":
   {
      "ngraph_cgiurl" : "http://nagios.fm4dd.com/nagios/cgi-bin/show.cgi",
      "rrd_basedir"   : "/srv/app/nagiosgraph/rrd",
      "graph_img_size": "521x60",
      "graph_bgcolor" : "#F2F2F2",
      "graph_border"  : "#000000"
   }
}
```

## Example Usage

### Host Notification Command

```nginx
# /etc/naemon/conf.d/commands/host-email-pnp4n-int-en.cfg
# $USER4$ is just a macro of where I put the pnp4_send_host_mail script
# You can substitute it for something like /usr/lib64/nagios/plugins

define command {
    command_name        host-email-pnp4n-int-en
    command_line        $USER4$/pnp4n_send_host_mail.pl \
-p "NTSG Annex, University of Montana" \
-c "$CONTACTADDRESS1$" \
--ent "$NOTIFICATIONTYPE$" \
--ena "$NOTIFICATIONAUTHOR$" \
--enc "$NOTIFICATIONCOMMENT$" \
--ehn "$HOSTNAME$" \
--eha "$HOSTALIAS$" \
--ehg "$HOSTGROUPNAME$" \
--ehd "$HOSTADDRESS$" \
--ehs "$HOSTSTATE$" \
--eho "$HOSTOUTPUT$" \
--etm "$LONGDATETIME$" \
--ece "$CONTACTEMAIL$" \
--ecr "$CONTACTGROUPMEMBERS$" \
-f graph -u -l en
}

```

### Service Notification Command

```nginx
# /etc/naemon/conf.d/commands/service-email-pnp4n-int-en.cfg
# $USER4$ is just a macro of where I put the pnp4_send_service_mail script
# You can substitute it for something like /usr/lib64/nagios/plugins

define command {
    command_name        service-email-pnp4n-int-en
    command_line        $USER4$/pnp4n_send_service_mail.pl \
-p "NTSG Annex, University of Montana" \
-c "$CONTACTADDRESS1$" \
--ehf "$HOSTSERVERFUNCTION$" \
--ent "$NOTIFICATIONTYPE$" \
--ena "$NOTIFICATIONAUTHOR$" \
--enc "$NOTIFICATIONCOMMENT$" \
--esd "$SERVICEDESC$" \
--ess "$SERVICESTATE$" \
--esg "$SERVICEGROUPNAME$" \
--ehn "$HOSTNAME$" \
--eha "$HOSTALIAS$" \
--ehg "$HOSTGROUPNAME$" \
--ehd "$HOSTADDRESS$" \
--eso "$SERVICEOUTPUT$" \
--etm "$LONGDATETIME$" \
--ece "$CONTACTEMAIL$" \
--ecr "$CONTACTGROUPMEMBERS$" \
-f graph -u -l en
}

```

## Additional Usage Information (from FORK)

To view a list of Naemon macro variables, look here:

http://www.naemon.org/documentation/usersguide/macrolist.html

### HOST SCRIPT

| Short | Long    | Nagios Environment Variable  | Naemon Macro Variable |
|-------|---------|------------------------------|-----------------------|
| `-T`  | `--ent` | `NAGIOS_NOTIFICATIONTYPE   ` | `NOTIFICATIONTYPE   ` |
| `-A`  | `--ena` | `NAGIOS_NOTIFICATIONAUTHOR ` | `NOTIFICATIONAUTHOR ` |
| `-C`  | `--enc` | `NAGIOS_NOTIFICATIONCOMMENT` | `NOTIFICATIONCOMMENT` |
| `-N`  | `--ehn` | `NAGIOS_HOSTNAME           ` | `HOSTNAME           ` |
| `-L`  | `--eha` | `NAGIOS_HOSTALIAS          ` | `HOSTALIAS          ` |
| `-G`  | `--ehg` | `NAGIOS_HOSTGROUPNAME      ` | `HOSTGROUPNAME      ` |
| `-D`  | `--ehd` | `NAGIOS_HOSTADDRESS        ` | `HOSTADDRESS        ` |
| `-S`  | `--ehs` | `NAGIOS_HOSTSTATE          ` | `HOSTSTATE          ` |
| `-O`  | `--eho` | `NAGIOS_HOSTOUTPUT         ` | `HOSTOUTPUT         ` |
| `-M`  | `--etm` | `NAGIOS_LONGDATETIME       ` | `LONGDATETIME       ` |
| `-E`  | `--ece` | `NAGIOS_CONTACTEMAIL       ` | `CONTACTEMAIL       ` |
| `-R`  | `--ecr` | `NAGIOS_CONTACTGROUPMEMBERS` | `CONTACTGROUPMEMBERS` |

### SERVICE SCRIPT

| Short | Long    | Nagios environment variable  | Naemon Macro Variable |
|-------|---------|------------------------------|-----------------------|
| `-F`  | `--ehf` | `NAGIOS_HOSTSERVERFUNCTION ` | `HOSTSERVERFUNCTION ` |
| `-T`  | `--ent` | `NAGIOS_NOTIFICATIONTYPE   ` | `NOTIFICATIONTYPE   ` |
| `-A`  | `--ena` | `NAGIOS_NOTIFICATIONAUTHOR ` | `NOTIFICATIONAUTHOR ` |
| `-C`  | `--enc` | `NAGIOS_NOTIFICATIONCOMMENT` | `NOTIFICATIONCOMMENT` |
| `-P`  | `--esd` | `NAGIOS_SERVICEDESC        ` | `SERVICEDESC        ` |
| `-S`  | `--ess` | `NAGIOS_SERVICESTATE       ` | `SERVICESTATE       ` |
| `-U`  | `--esg` | `NAGIOS_SERVICEGROUPNAME   ` | `SERVICEGROUPNAME   ` |
| `-N`  | `--ehn` | `NAGIOS_HOSTNAME           ` | `HOSTNAME           ` |
| `-L`  | `--eha` | `NAGIOS_HOSTALIAS          ` | `HOSTALIAS          ` |
| `-G`  | `--ehg` | `NAGIOS_HOSTGROUPNAME      ` | `HOSTGROUPNAME      ` |
| `-D`  | `--ehd` | `NAGIOS_HOSTADDRESS        ` | `HOSTADDRESS        ` |
| `-O`  | `--eso` | `NAGIOS_SERVICEOUTPUT      ` | `SERVICEOUTPUT      ` |
| `-M`  | `--etm` | `NAGIOS_LONGDATETIME       ` | `LONGDATETIME       ` |
| `-E`  | `--ece` | `NAGIOS_CONTACTEMAIL       ` | `CONTACTEMAIL       ` |
| `-R`  | `--ecr` | `NAGIOS_CONTACTGROUPMEMBERS` | `CONTACTGROUPMEMBERS` |

## Original Usage

```
nagios_send_host_mail.pl [-v] [-V] [-h] [-t] [-H <SMTP host>] [-p] 
[-r to_<recipients>] [-c <cc_recipients>] [-b <bcc_recipients>] [-f <text|html|multi|graph>] [-u] [-l <en|jp|fr|de>]
```

#### Options:

```

-v, --verbose  
      print extra debugging information

-V, --version  
      prints version number

-h, --help  
      shows the programs help and options

-H, --smtphost=HOST  
      name or IP address of SMTP gateway

-t, --test  
      creates a test notification e-mail

-p, --customer  
      adds the customer or company name header

-r, --to-recipients  
      this argument overrides the Nagios-provided $CONTACTEMAIL$ list of to: recipients. It is good to use together with the -t/--test option

-c, --cc-recipients  
      to add cc: recipients, add this argument and set it to receive the Nagios-provided $CONTACTADDRESS1$ list

-b, --bcc-recipients  
      the Nagios-provided $CONTACTADDRESS2$ list of bcc: recipients

-f, --format='text|html|multi|graph'
      the email format to generate: either plain text, HTML, multipart S/Mime ouput with a inline logo and graph adds the Nagiosgraph performance image if available

-u, --addurl  
      this adds URL's to the Nagios web GUI for check status, host and hostgroup views into the html mail, requires -f html, -f multi or -f graph

-l, --language='en|jp|fr|de'
      the prefered e-mail language. The content-type header is currently is hard-coded to UTF-8. This might need to be changed if recipients require a different characterset encoding.
```

## Nagios flexible notification scripts

Nagios Core only comes with a barebone structure for plaintext-based e-mail alerting. 
These add-on scripts take the notification further by adding HTML formatting, color coding, language selections, and more.
Integration with two popular graphing packages Nagiosgraph and PNP4Nagios adds problem visualization to the alerts.

This shortens the time for problem identification, increases problem transparency, and decreases time-to-fix.

### Service Notification Example:

![Alt text](http://nagios.fm4dd.com/howto/images/notification-graph-en-service-crit1.png "Service Notification Example")

### Notes

See also:

http://nagios.fm4dd.com/howto/nagios-flexible-notifications.htm

http://nagios.fm4dd.com/howto/manual/nagios_send_host_mail.htm

http://nagios.fm4dd.com/howto/manual/nagios_send_service_mail.htm

http://nagios.fm4dd.com/howto/manual/pnp4n_send_service_mail.htm

https://exchange.nagios.org/directory/Addons/Notifications/multi-2Dlanguage-HTML-mail-alert-generation-for-Nagios/details
