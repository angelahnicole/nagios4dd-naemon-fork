# nagios4dd FORK 

## Overview

 Below is the documentation for the collection of scripts as a whole, but note that this repository has been cloned to modify the **notification** scripts to work with Naemon. The biggest change being that Naemon does not allow you to enable macros, and so all of the variables expected to be environment variables in the script is now passed in.

Again, changes have only been made for the **notification** scripts.

## Checkout only the **notifications** directory

Since only the **notifications** directory was changed, we probably only want to check out that directory when using it. To do this, we accomplish what's called a *sparse-checkout*.

```
$ git init
$ git remote add -f origin https://bitbucket.org/ntsg_annex/nagios4dd-naemon-fork
$ git config core.sparsecheckout true
$ echo notifications/ >> .git/info/sparse-checkout
$ git pull origin master
```

For more information, visit: 

http://jasonkarns.com/blog/subdirectory-checkouts-with-git-sparse-checkout/

------------------------------------------------------------------------------------------

## nagios4dd

This repository contains Nagios-related software. Plugins and plugin enhancements, tools and notification scripts.

### Nagios flexible notification scripts

[notifications](notifications): Nagios Core only comes with a barebone structure for plaintext-based e-mail alerting. 
These add-on scripts take the notification further by adding HTML formatting, color coding, language selections, and more.
Integration with two popular graphing packages Nagiosgraph and PNP4Nagios adds problem visualization to the alerts.

This shortens the time for problem identification, increases problem transparency, and decreases time-to-fix.

See also: http://nagios.fm4dd.com/howto/nagios-flexible-notifications.htm

Service Notification Example:
![Alt text](http://nagios.fm4dd.com/howto/images/notification-graph-en-service-crit1.png "Service Notification Example")

### Nagios monitoring plugins

[plugins](plugins): A set of 30+ plugins that covers a variety of use cases, such as database monitoring and security related indicators.


### Nagios monitoring tools

[tools](tools): Here I am keeping tools for creation and management of Nagios monitoring configurations.